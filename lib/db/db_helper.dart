import 'package:food_agenda/models/habit.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static Database? _db;
  static final int _version = 1;
  static final String _tableName = 'habits';

  // init method
  static Future<void> initDb() async {
    if (_db != null) {
      return;
    }
    try {
      String _path = await getDatabasesPath() + 'habits.db';
      _db = await openDatabase(
        _path,
        version: _version,
        onCreate: (db, version) {
          print('Creating a new one');
          return db.execute(
            "CREATE TABLE $_tableName ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "date STRING, time STRING, food STRING, numberEaten INTEGER, "
            "numberBowel INTEGER, quantityWater INTEGER, "
            "quantityOtherLiquid INTEGER, eatingFruits INTEGER, "
            "healthProblems INTEGER, color INTEGER)",
          );
        },
      );
    } catch (e) {
      print(e);
    }
  }

  // insert method
  static Future<int> insert(Habit? habit) async {
    print("Insert function called");
    return await _db?.insert(_tableName, habit!.toJson()) ?? 1;
  }

  // query method
  static Future<List<Map<String, dynamic>>> query() async {
    print("Query function called");
    return await _db!.query(_tableName);
  }

  // delete method
  static delete(Habit habit) async {
    return await _db!.delete(_tableName, where: 'id=?', whereArgs: [habit.id]);
  }
}
