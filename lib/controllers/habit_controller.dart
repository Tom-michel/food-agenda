import 'package:food_agenda/db/db_helper.dart';
import 'package:food_agenda/models/habit.dart';
import 'package:get/get.dart';

class HabitController extends GetxController {
  @override
  void onReady() {
    super.onReady();
  }

  var habitList = <Habit>[].obs;

  Future<int> addHabit({Habit? habit}) async {
    return await DBHelper.insert(habit);
  }

  // get all the date from table
  void getHabits() async {
    List<Map<String, dynamic>> habits = await DBHelper.query();
    habitList
        .assignAll(habits.map((data) => new Habit.fromJson(data)).toList());
  }

  // delete method
  void delete(Habit habit) {
    DBHelper.delete(habit);
  }
}
