import 'package:flutter/material.dart';
import 'package:food_agenda/controllers/habit_controller.dart';
import 'package:food_agenda/models/habit.dart';
import 'package:food_agenda/services/theme_services.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:food_agenda/ui/widgets/functions.dart';
import 'package:food_agenda/ui/widgets/input_field.dart';
import 'package:get/get.dart';

class DashbordPage extends StatefulWidget {
  const DashbordPage({super.key});

  @override
  State<DashbordPage> createState() => _DashbordPageState();
}

class _DashbordPageState extends State<DashbordPage> {
  final _habitController = Get.put(HabitController());

  String? selectedTime;
  DateTime _selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: context.theme.backgroundColor,
        backgroundColor: Get.isDarkMode ? null : bgHomeColor,
        appBar: _appBar(context),
        body: Column(
          children: [
            addDateBar(_selectedDate),
            _showHabits(),
          ],
        ));
  }

  _showHabits() {
    return Expanded(
      child: Obx(() {
        return ListView.builder(
          itemCount: _habitController.habitList.length,
          itemBuilder: (_, index) {
            // print(_habitController.habitList.length);
            // return Container(
            //   width: 100,
            //   height: 50,
            //   color: Colors.green,
            //   margin: const EdgeInsets.only(bottom: 10),
            //   child: Text(_habitController.habitList[index].food.toString()),
            // );
            return GestureDetector(
              onTap: () {
                // _habitController.delete(_habitController.habitList[index]);
                // _habitController.getHabits();
                _showBottomSheet(context, _habitController.habitList[index]);
              },
              child: Container(
                margin: const EdgeInsets.only(top: 40, left: 20, right: 20),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                decoration: BoxDecoration(
                  // color: Colors.white,
                  color: context.theme.backgroundColor,
                  borderRadius: BorderRadius.circular(6),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 10,
                    ),
                  ],
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.fastfood_outlined,
                          color: primaryClr,
                          size: 30,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          _habitController.habitList[index].food
                              .toString()
                              .toUpperCase(),
                          style: titleHabitStyle,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Column(
                      children: [
                        buildLineDetailHabit(
                          index,
                          Icons.calendar_today_outlined,
                          _habitController.habitList[index].date.toString() +
                              " at " +
                              _habitController.habitList[index].time.toString(),
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.food_bank_outlined,
                          _habitController.habitList[index].numberEaten
                                  .toString() +
                              " times eaten",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.emoji_food_beverage_outlined,
                          _habitController.habitList[index].numberBowel
                                  .toString() +
                              " times bowel movement",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.water_drop_outlined,
                          _habitController.habitList[index].quantityWater
                                  .toString() +
                              " L water drank",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.emoji_food_beverage_outlined,
                          _habitController.habitList[index].quantityOtherLiquid
                                  .toString() +
                              " L other liquid",
                        ),
                        buildLineDetailHabit(
                            index,
                            Icons.apple_outlined,
                            _habitController.habitList[index].eatingFruits
                                        .toString() ==
                                    "1"
                                ? "YES"
                                : "NO"),
                        buildLineDetailHabit(
                          index,
                          Icons.healing_outlined,
                          _habitController.habitList[index].healthProblems
                                      .toString() ==
                                  "1"
                              ? "YES"
                              : "NO",
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        );
      }),
    );
  }

  _bottomShettButton({
    required String label,
    required Function()? onTap,
    required Color color,
    bool isClose = false,
    required BuildContext context,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        height: 55,
        width: MediaQuery.of(context).size.width * 0.9,
        decoration: BoxDecoration(
            color: isClose == true ? color : Colors.red,
            border: Border.all(
                width: 2, color: isClose == true ? darkHeaderClr : Colors.red),
            borderRadius: BorderRadius.circular(6)),
        child: Center(
          child: Text(
            label,
            style: isClose
                ? titleStyle.copyWith(color: darkHeaderClr)
                : titleStyle.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  _showBottomSheet(BuildContext context, Habit habit) {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.only(top: 4),
        height: MediaQuery.of(context).size.height * 0.24,
        color: Get.isDarkMode ? darkGreyClr : Colors.white,
        child: Column(
          children: [
            Container(
              height: 6,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Get.isDarkMode ? Colors.grey[500] : Colors.grey[300],
              ),
            ),
            Spacer(),
            _bottomShettButton(
              label: "Delete Habit",
              onTap: () {
                _habitController.delete(habit);
                _habitController.getHabits();
                Get.back();
              },
              color: primaryClr,
              context: context,
            ),
            SizedBox(height: 20),
            _bottomShettButton(
              label: "Close",
              onTap: () {
                Get.back();
              },
              color: Colors.white,
              isClose: true,
              context: context,
            ),
          ],
        ),
      ),
    );
  }

  Container buildLineDetailHabit(int index, IconData icon, String text) {
    return Container(
      // color: Get.isDarkMode ? darkHeaderClr : Colors.grey.shade200,
      color: Get.isDarkMode ? darkHeaderClr : Colors.grey.shade200,
      margin: const EdgeInsets.only(top: 5, left: 10, right: 10),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Row(
        children: [
          Icon(icon,
              color: Get.isDarkMode ? Colors.grey[400] : Colors.grey[600]),
          const SizedBox(width: 10),
          Text(
            text,
            style: tileTitleStyle,
          ),
        ],
      ),
    );
  }

  ListView builLineHabit() {
    return ListView.builder(
      itemCount: 7,
      itemBuilder: (context, index) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: context.theme.backgroundColor,
              borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  const SizedBox(width: 20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Habit Name",
                        style: headingStyle,
                      ),
                      const SizedBox(height: 5),
                      Text(
                        "Habit Description",
                        style: subheadingStyle,
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "10",
                    style: headingStyle,
                  ),
                  const SizedBox(width: 5),
                  Text(
                    "Days",
                    style: subheadingStyle,
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: context.theme.backgroundColor,
      elevation: 0,
      leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Icon(Icons.arrow_back_ios,
            size: 20, color: Get.isDarkMode ? Colors.white : Colors.black),
      ),
      title: Text(
        "Dashbord",
        style: headingStyle,
      ),
      actions: [
        GestureDetector(
          onTap: () {
            ThemeServices().switchTheme();
          },
          child: Icon(
              Get.isDarkMode ? Icons.wb_sunny_outlined : Icons.nightlight_round,
              size: 25,
              color: Get.isDarkMode ? Colors.yellow : Colors.black),
        ),
        const SizedBox(width: 20)
      ],
    );
  }
}
