import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_agenda/controllers/habit_controller.dart';
import 'package:food_agenda/models/habit.dart';
import 'package:food_agenda/services/theme_services.dart';
import 'package:food_agenda/ui/add_habbit_page.dart';
import 'package:food_agenda/ui/dashbord_page.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:food_agenda/ui/widgets/button.dart';
import 'package:food_agenda/ui/widgets/functions.dart';
import 'package:food_agenda/ui/widgets/input_field.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/default_transitions.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<AnimatedListState> _key = GlobalKey();

  final TextEditingController _controller = TextEditingController();

  final items = [];

  Future<void> addDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Add New Food"),
          content: TextField(
            controller: _controller,
            decoration: const InputDecoration(
              hintText: "E.g Chiken DG",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                addItem(_controller.text);
                Navigator.pop(context);
              },
              child: const Text("Add +"),
            ),
          ],
        );
      },
    );
  }

  void addItem(String title) {
    items.insert(0, title);
    _key.currentState!
        .insertItem(0, duration: const Duration(milliseconds: 500));
  }

  void removeIem(int index, BuildContext context) {
    AnimatedList.of(context).removeItem(index, (context, animation) {
      return FadeTransition(
        opacity: animation,
        child: SizeTransition(
          sizeFactor: animation,
          child: Card(
            elevation: 0,
            color: Colors.grey.shade200,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            child: ListTile(
              title: Text(items[index], style: tileTitleStyle),
              trailing: IconButton(
                onPressed: () => removeIem(index, context),
                icon: const Icon(
                  Icons.delete,
                ),
              ),
            ),
          ),
        ),
      );
    });
  }

  DateTime _selectedDate = DateTime.now();
  final _habitController = Get.put(HabitController());

  @override
  void initState() {
    super.initState();
    _habitController.getHabits();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      backgroundColor: Get.isDarkMode ? null : bgHomeColor,
      body: Column(
        children: [
          // _addDateBar(),
          _showHabits(),
        ],
      ),
    );
  }

  _showHabits() {
    return Expanded(
      child: Obx(() {
        return ListView.builder(
          itemCount: _habitController.habitList.length,
          itemBuilder: (_, index) {
            return GestureDetector(
              onTap: () {
                // _habitController.delete(_habitController.habitList[index]);
                // _habitController.getHabits();
                _showBottomSheet(context, _habitController.habitList[index]);
              },
              child: Container(
                margin: const EdgeInsets.only(top: 40, left: 20, right: 20),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                decoration: BoxDecoration(
                  // color: Colors.white,
                  color: context.theme.backgroundColor,
                  borderRadius: BorderRadius.circular(6),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 10,
                    ),
                  ],
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.fastfood_outlined,
                          color: primaryClr,
                          size: 30,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          _habitController.habitList[index].food
                              .toString()
                              .toUpperCase(),
                          style: titleHabitStyle,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Column(
                      children: [
                        buildLineDetailHabit(
                          index,
                          Icons.calendar_today_outlined,
                          _habitController.habitList[index].date.toString() +
                              " at " +
                              _habitController.habitList[index].time.toString(),
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.food_bank_outlined,
                          _habitController.habitList[index].numberEaten
                                  .toString() +
                              " times eaten",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.emoji_food_beverage_outlined,
                          _habitController.habitList[index].numberBowel
                                  .toString() +
                              " times bowel movement",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.water_drop_outlined,
                          _habitController.habitList[index].quantityWater
                                  .toString() +
                              " L water drank",
                        ),
                        buildLineDetailHabit(
                          index,
                          Icons.emoji_food_beverage_outlined,
                          _habitController.habitList[index].quantityOtherLiquid
                                  .toString() +
                              " L other liquid",
                        ),
                        buildLineDetailHabit(
                            index,
                            Icons.apple_outlined,
                            _habitController.habitList[index].eatingFruits
                                        .toString() ==
                                    "1"
                                ? "YES"
                                : "NO"),
                        buildLineDetailHabit(
                          index,
                          Icons.healing_outlined,
                          _habitController.habitList[index].healthProblems
                                      .toString() ==
                                  "1"
                              ? "YES"
                              : "NO",
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        );
      }),
    );
  }

  Container buildLineDetailHabit(int index, IconData icon, String text) {
    return Container(
      // color: Get.isDarkMode ? darkHeaderClr : Colors.grey.shade200,
      color: Get.isDarkMode ? darkHeaderClr : Colors.grey.shade200,
      margin: const EdgeInsets.only(top: 5, left: 10, right: 10),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Row(
        children: [
          Icon(icon,
              color: Get.isDarkMode ? Colors.grey[400] : Colors.grey[600]),
          const SizedBox(width: 10),
          Text(
            text,
            style: tileTitleStyle,
          ),
        ],
      ),
    );
  }

  _bottomShettButton({
    required String label,
    required Function()? onTap,
    required Color color,
    bool isClose = false,
    required BuildContext context,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        height: 55,
        width: MediaQuery.of(context).size.width * 0.9,
        decoration: BoxDecoration(
            color: isClose == true ? color : Colors.red,
            border: Border.all(
                width: 2, color: isClose == true ? darkHeaderClr : Colors.red),
            borderRadius: BorderRadius.circular(6)),
        child: Center(
          child: Text(
            label,
            style: isClose
                ? titleStyle.copyWith(color: darkHeaderClr)
                : titleStyle.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  _showBottomSheet(BuildContext context, Habit habit) {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.only(top: 4),
        height: MediaQuery.of(context).size.height * 0.24,
        color: Get.isDarkMode ? darkGreyClr : Colors.white,
        child: Column(
          children: [
            Container(
              height: 6,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Get.isDarkMode ? Colors.grey[500] : Colors.grey[300],
              ),
            ),
            Spacer(),
            _bottomShettButton(
              label: "Delete Habit",
              onTap: () {
                _habitController.delete(habit);
                _habitController.getHabits();
                Get.back();
              },
              color: primaryClr,
              context: context,
            ),
            SizedBox(height: 20),
            _bottomShettButton(
              label: "Close",
              onTap: () {
                Get.back();
              },
              color: Colors.white,
              isClose: true,
              context: context,
            ),
          ],
        ),
      ),
    );
  }

  Container _addDateBar() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 20),
      child: DatePicker(
        DateTime.now(),
        height: 100,
        width: 80,
        initialSelectedDate: DateTime.now(),
        selectionColor: primaryClr,
        selectedTextColor: Colors.white,
        // dateTextStyle: const TextStyle(
        //   fontSize: 20,
        //   fontWeight: FontWeight.w600,
        //   color: Colors.grey,
        // ),
        dateTextStyle: GoogleFonts.lato(
          textStyle: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
            color: Colors.grey,
          ),
        ),
        dayTextStyle: GoogleFonts.lato(
          textStyle: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: Colors.grey,
          ),
        ),
        monthTextStyle: GoogleFonts.lato(
          textStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w600,
            color: Colors.grey,
          ),
        ),
        onDateChange: (selectedDate) {
          _selectedDate = selectedDate;
        },
      ),
    );
  }

  Container addHabitBar() {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                DateFormat.yMMMEd().format(DateTime.now()),
                style: subheadingStyle,
              ),
              Text(
                "Today",
                style: headingStyle,
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.to(const DashbordPage());
                    },
                    child: Icon(Icons.calendar_month_outlined,
                        size: 25,
                        color: Get.isDarkMode ? Colors.grey[400] : primaryClr),
                  ),
                  const SizedBox(width: 12),
                  GestureDetector(
                    onTap: () {
                      ThemeServices().switchTheme();
                    },
                    child: Icon(
                        Get.isDarkMode
                            ? Icons.wb_sunny_outlined
                            : Icons.nightlight_round,
                        size: 25,
                        color: Get.isDarkMode ? Colors.yellow : Colors.black),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              MyButton(
                label: "+ Add Habbit",
                onTap: () async {
                  await Get.to(const AddHabitPage());
                  _habitController.getHabits();
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  AppBar appBar() {
    return AppBar(
      toolbarHeight: 90,
      backgroundColor: context.theme.backgroundColor,
      elevation: 0,
      flexibleSpace: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 20),
          addHabitBar(),
        ],
      ),
    );
  }
}
