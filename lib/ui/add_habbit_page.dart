import 'package:flutter/material.dart';
import 'package:food_agenda/controllers/habit_controller.dart';
import 'package:food_agenda/models/habit.dart';
import 'package:food_agenda/services/theme_services.dart';
import 'package:food_agenda/ui/dashbord_page.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:food_agenda/ui/widgets/button.dart';
import 'package:food_agenda/ui/widgets/input_field.dart';
import 'package:food_agenda/ui/widgets/input_field_temp.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddHabitPage extends StatefulWidget {
  const AddHabitPage({super.key});

  @override
  State<AddHabitPage> createState() => _AddHabitPageState();
}

class _AddHabitPageState extends State<AddHabitPage> {
  final HabitController _habitController = Get.put(HabitController());
  final TextEditingController _foodController = TextEditingController();

  DateTime _selectedDate = DateTime.now();
  String _startTime = DateFormat("hh:mm a").format(DateTime.now()).toString();
  String _selectedTime =
      DateFormat("hh:mm a").format(DateTime.now()).toString();

  int _numberEaten = 1;
  List<int> numberEatenList = [1, 2, 3, 4, 5];

  int _numberBowel = 0;
  List<int> numberBowelList = [0, 1, 2, 3, 4, 5];

  int _quantityWater = 0;
  List<int> quantityWaterList = [0, 1, 2, 3, 4, 5];

  int _quantityOtherLiquid = 0;
  List<int> quantityOtherLiquidList = [0, 1, 2, 3, 4, 5];

  int _eatingFruits = 0;
  int _healthProblems = 0;
  bool _eatingFruitsBool = false;
  bool _healthProblemsBool = false;

  int _selectedColor = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.backgroundColor,
      appBar: _appBar(context),
      body: Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 10),
              // ajouter un nouveau repas
              Text("Add a new eaten food", style: subheadingStyle),
              const SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Date",
                    hint: DateFormat.yMd().format(_selectedDate),
                    widget: IconButton(
                      onPressed: () {
                        _getDateFromUser();
                      },
                      icon: const Icon(Icons.calendar_today_outlined,
                          color: Colors.grey),
                    ),
                  )),
                  const SizedBox(width: 12),
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Time",
                    hint: _selectedTime != null ? '$_selectedTime' : _startTime,
                    widget: IconButton(
                      onPressed: () {
                        displayTimeDialog();
                      },
                      icon: const Icon(Icons.access_time_rounded),
                      color: Colors.grey,
                    ),
                  )),
                ],
              ),
              MyInputFieldTemp(
                title: "Food eaten",
                hint: "Enter the name of food",
                controller: _foodController,
              ),
              Row(
                children: [
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Number time eaten",
                    hint: _numberEaten <= 1
                        ? "$_numberEaten time"
                        : "$_numberEaten times",
                    widget: DropdownButton(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          color: Colors.grey),
                      iconSize: 32,
                      elevation: 4,
                      style: subtitleStyle,
                      underline: Container(height: 0),
                      onChanged: (String? newValue) {
                        setState(() {
                          _numberEaten = int.parse(newValue!);
                        });
                      },
                      items: numberEatenList
                          .map<DropdownMenuItem<String>>((int value) {
                        return DropdownMenuItem<String>(
                          value: value.toString(),
                          child: Text(value.toString()),
                        );
                      }).toList(),
                    ),
                  )),
                  const SizedBox(width: 12),
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Nomber bowel",
                    hint: _numberBowel <= 1
                        ? "$_numberBowel time"
                        : "$_numberBowel times",
                    widget: DropdownButton(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          color: Colors.grey),
                      iconSize: 32,
                      elevation: 4,
                      style: subtitleStyle,
                      underline: Container(height: 0),
                      onChanged: (String? newValue) {
                        setState(() {
                          _numberBowel = int.parse(newValue!);
                        });
                      },
                      items: numberBowelList
                          .map<DropdownMenuItem<String>>((int value) {
                        return DropdownMenuItem<String>(
                          value: value.toString(),
                          child: Text(value.toString()),
                        );
                      }).toList(),
                    ),
                  )),
                ],
              ),
              // const MyInputFieldTemp(
              //     title: "Health problems",
              //     hint: "Health problems caused by this food"),
              Row(
                children: [
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Quantity water",
                    hint: _quantityWater <= 1
                        ? "$_quantityWater liter"
                        : "$_quantityWater liters",
                    widget: DropdownButton(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          color: Colors.grey),
                      iconSize: 32,
                      elevation: 4,
                      style: subtitleStyle,
                      underline: Container(height: 0),
                      onChanged: (String? newValue) {
                        setState(() {
                          _quantityWater = int.parse(newValue!);
                        });
                      },
                      items: quantityWaterList
                          .map<DropdownMenuItem<String>>((int value) {
                        return DropdownMenuItem<String>(
                          value: value.toString(),
                          child: Text(value.toString()),
                        );
                      }).toList(),
                    ),
                  )),
                  const SizedBox(width: 12),
                  Expanded(
                      child: MyInputFieldTemp(
                    title: "Quantity other liquid",
                    hint: _quantityOtherLiquid <= 1
                        ? "$_quantityOtherLiquid liter"
                        : "$_quantityOtherLiquid liters",
                    widget: DropdownButton(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          color: Colors.grey),
                      iconSize: 32,
                      elevation: 4,
                      style: subtitleStyle,
                      underline: Container(height: 0),
                      onChanged: (String? newValue) {
                        setState(() {
                          _quantityOtherLiquid = int.parse(newValue!);
                        });
                      },
                      items: quantityOtherLiquidList
                          .map<DropdownMenuItem<String>>((int value) {
                        return DropdownMenuItem<String>(
                          value: value.toString(),
                          child: Text(value.toString()),
                        );
                      }).toList(),
                    ),
                  )),
                ],
              ),
              // const MyInputFieldTemp(
              //     title: "Eating fruits and legumes",
              //     hint: "Enter the name of eaten fruits"),
              Column(
                children: [
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      Text("Eating fruits and legumes ?", style: titleStyle),
                      Checkbox(
                          activeColor: primaryClr,
                          value: _eatingFruitsBool,
                          onChanged: (bool? value) {
                            setState(() {
                              _eatingFruitsBool = value!;
                              _eatingFruits = value == true ? 1 : 0;
                            });
                          })
                    ],
                  ),
                  Row(
                    children: [
                      Text("Health problems ?", style: titleStyle),
                      Checkbox(
                          activeColor: primaryClr,
                          value: _healthProblemsBool,
                          onChanged: (bool? value) {
                            setState(() {
                              _healthProblemsBool = value!;
                              _healthProblems = value == true ? 1 : 0;
                            });
                          })
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 18),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _colorPalete(),
                  MyButton(
                    label: "+ Add Habbit",
                    onTap: () => _validateData(),
                  )
                ],
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  _validateData() {
    if (_foodController.text.isNotEmpty) {
      _addHabitToDb();
      Get.back();
    } else if (_foodController.text.isEmpty) {
      Get.snackbar(
        "Required",
        "All firlds are require !",
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: darkHeaderClr,
        icon: Icon(
          Icons.warning_amber_rounded,
          color: primaryClr,
          size: 30,
        ),
        colorText: Colors.white,
      );
    }
  }

  _addHabitToDb() async {
    int value = await _habitController.addHabit(
        habit: Habit(
      date: DateFormat.yMd().format(_selectedDate),
      time: _selectedTime,
      food: _foodController.text,
      numberEaten: _numberEaten,
      numberBowel: _numberBowel,
      quantityWater: _quantityWater,
      quantityOtherLiquid: _quantityOtherLiquid,
      eatingFruits: _eatingFruits,
      healthProblems: _healthProblems,
      color: _selectedColor,
    ));
    print("My id is " + "$value");
  }

  Column _colorPalete() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Color", style: titleStyle),
        const SizedBox(height: 8.0),
        Wrap(
          children: List<Widget>.generate(
            3,
            (index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedColor = index;
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: CircleAvatar(
                    radius: 14,
                    backgroundColor: index == 0
                        ? primaryClr
                        : index == 1
                            ? pinkClr
                            : bluishClr,
                    child: _selectedColor == index
                        ? const Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 16,
                          )
                        : Container(),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: context.theme.backgroundColor,
      elevation: 0,
      leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Icon(Icons.arrow_back_ios,
            size: 20, color: Get.isDarkMode ? Colors.white : Colors.black),
      ),
      title: Text(
        "Add Habit",
        style: headingStyle,
      ),
      actions: [
        Row(
          children: [
            GestureDetector(
              onTap: () {
                Get.to(const DashbordPage());
              },
              child: Icon(Icons.calendar_month_outlined,
                  size: 25,
                  color: Get.isDarkMode ? Colors.grey[400] : primaryClr),
            ),
            const SizedBox(width: 12),
            GestureDetector(
              onTap: () {
                ThemeServices().switchTheme();
              },
              child: Icon(
                  Get.isDarkMode
                      ? Icons.wb_sunny_outlined
                      : Icons.nightlight_round,
                  size: 25,
                  color: Get.isDarkMode ? Colors.yellow : Colors.black),
            ),
            const SizedBox(width: 20)
          ],
        ),
      ],
    );
  }

  _getDateFromUser() async {
    DateTime? _pickerDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (_pickerDate != null) {
      setState(() {
        _selectedDate = _pickerDate;
      });
    } else {
      print("It's null or something is wrong");
    }
  }

  Future<void> displayTimeDialog() async {
    final TimeOfDay? time =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (time != null) {
      setState(() {
        _selectedTime = time.format(context);
      });
    }
  }
}
