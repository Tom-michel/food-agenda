// import 'package:flutter/material.dart';
// import 'package:food_agenda/models/habit.dart';
// import 'package:food_agenda/ui/theme.dart';

// class HabitTile extends StatelessWidget {
//   const HabitTile({super.key, this.habit});

//   final Habit? habit;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: 40, left: 20, right: 20),
//       padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//       decoration: BoxDecoration(
//         // color: Colors.white,
//         // color: context.theme.backgroundColor,
//         borderRadius: BorderRadius.circular(6),
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black.withOpacity(0.1),
//             blurRadius: 10,
//           ),
//         ],
//         border: Border.all(
//           color: Colors.grey.withOpacity(0.2),
//         ),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Row(
//             children: [
//               Icon(
//                 Icons.fastfood_outlined,
//                 color: primaryClr,
//                 size: 30,
//               ),
//               const SizedBox(width: 10),
//               Text(
//                 _habitController.habitList[index].food.toString().toUpperCase(),
//                 style: titleHabitStyle,
//               ),
//             ],
//           ),
//           const SizedBox(height: 10),
//           Column(
//             children: [
//               buildLineDetailHabit(
//                 index,
//                 Icons.calendar_today_outlined,
//                 _habitController.habitList[index].date.toString() +
//                     " at " +
//                     _habitController.habitList[index].time.toString(),
//               ),
//               buildLineDetailHabit(
//                 index,
//                 Icons.food_bank_outlined,
//                 _habitController.habitList[index].numberEaten.toString() +
//                     " times eaten",
//               ),
//               buildLineDetailHabit(
//                 index,
//                 Icons.emoji_food_beverage_outlined,
//                 _habitController.habitList[index].numberBowel.toString() +
//                     " times bowel movement",
//               ),
//               buildLineDetailHabit(
//                 index,
//                 Icons.water_drop_outlined,
//                 _habitController.habitList[index].quantityWater.toString() +
//                     " times bowel movement",
//               ),
//               buildLineDetailHabit(
//                 index,
//                 Icons.emoji_food_beverage_outlined,
//                 _habitController.habitList[index].quantityOtherLiquid
//                         .toString() +
//                     " times bowel movement",
//               ),
//               buildLineDetailHabit(
//                   index,
//                   Icons.apple_outlined,
//                   _habitController.habitList[index].eatingFruits.toString() ==
//                           "1"
//                       ? "YES"
//                       : "NO"),
//               buildLineDetailHabit(
//                 index,
//                 Icons.healing_outlined,
//                 _habitController.habitList[index].healthProblems.toString() ==
//                         "1"
//                     ? "YES"
//                     : "NO",
//               ),
//             ],
//           )
//         ],
//       ),
//     );
//     ;
//   }
// }
