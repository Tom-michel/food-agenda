// body: Container(
//         color: Get.isDarkMode ? darkHeaderClr : bgHomeColor,
//         child: SingleChildScrollView(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               // foods
//               Container(
//                 margin: const EdgeInsets.only(top: 16, left: 20, right: 20),
//                 padding:
//                     const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//                 decoration: BoxDecoration(
//                   color: context.theme.backgroundColor,
//                   borderRadius: BorderRadius.circular(6),
//                   boxShadow: [
//                     BoxShadow(
//                       color: Colors.black.withOpacity(0.1),
//                       blurRadius: 10,
//                     ),
//                   ],
//                   border: Border.all(
//                     color: Colors.grey.withOpacity(0.2),
//                   ),
//                 ),
//                 child: Column(
//                   children: [
//                     // title
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Row(
//                           children: [
//                             Icon(Icons.food_bank_outlined, color: primaryClr),
//                             const SizedBox(width: 10),
//                             Text("Food eaten", style: titleStyle),
//                           ],
//                         ),
//                         GestureDetector(
//                             onTap: () {
//                               _controller.clear();
//                               addDialog(context);
//                             },
//                             child: const Icon(Icons.add_circle_outline))
//                       ],
//                     ),
//                     // list
//                     SizedBox(
//                       height: 100,
//                       child: Expanded(
//                         child: Padding(
//                           padding: const EdgeInsets.all(10),
//                           child: AnimatedList(
//                             key: _key,
//                             initialItemCount: items.length,
//                             itemBuilder: (context, index, animation) {
//                               return FadeTransition(
//                                 opacity: animation,
//                                 child: SizeTransition(
//                                   sizeFactor: animation,
//                                   key: UniqueKey(),
//                                   child: Card(
//                                     elevation: 0,
//                                     color: Get.isDarkMode
//                                         ? darkHeaderClr
//                                         : Colors.grey.shade200,
//                                     shape: RoundedRectangleBorder(
//                                       borderRadius: BorderRadius.circular(10),
//                                     ),
//                                     child: ListTile(
//                                       title: Text(items[index],
//                                           style: tileTitleStyle),
//                                       // subtitle: Text(DateTime.now().toString().substring(0, 10),style: const TextStyle(fontSize: 12)),
//                                       trailing: IconButton(
//                                         onPressed: () =>
//                                             removeIem(index, context),
//                                         icon: const Icon(
//                                           Icons.delete,
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               );
//                             },
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Column(
//                 children: const [
//                   MyInputField(
//                       icon: Icons.numbers,
//                       title: "Number time food eaten",
//                       hint: "Enter a number"),
//                   MyInputField(
//                       icon: Icons.water_drop_outlined,
//                       title: "Quantity of water drank",
//                       hint: "Enter a number"),
//                   MyInputField(
//                       icon: Icons.water_rounded,
//                       title: "Quantity of other liquids drank",
//                       hint: "Enter a number"),
//                   MyInputField(
//                       icon: Icons.wc_rounded,
//                       title: "Number bowel movements",
//                       hint: "Enter a number"),
//                 ],
//               ),
//               const SizedBox(height: 20),
//             ],
//           ),
//         ),
//       ),