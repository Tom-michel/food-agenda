import 'package:flutter/material.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:food_agenda/ui/widgets/input_field.dart';
import 'package:food_agenda/ui/widgets/input_field_temp.dart';

class AddHabitForm extends StatefulWidget {
  const AddHabitForm({super.key});

  @override
  State<AddHabitForm> createState() => _AddHabitFormState();
}

class _AddHabitFormState extends State<AddHabitForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Add Habit",
              style: headingStyle,
            ),
            const MyInputFieldTemp(title: "Title", hint: "Enter your title"),
          ],
        ),
      ),
    );
  }
}
