class Habit {
  int? id;
  String? date;
  String? time;
  String? food;
  int? numberEaten;
  int? numberBowel;
  int? quantityWater;
  int? quantityOtherLiquid;
  int? eatingFruits;
  int? healthProblems;
  int? color;

  Habit({
    this.id,
    this.date,
    this.time,
    this.food,
    this.numberEaten,
    this.numberBowel,
    this.quantityWater,
    this.quantityOtherLiquid,
    this.eatingFruits,
    this.healthProblems,
    this.color,
  });

  Habit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    time = json['time'];
    food = json['food'];
    numberEaten = json['numberEaten'];
    numberBowel = json['numberBowel'];
    quantityWater = json['quantityWater'];
    quantityOtherLiquid = json['quantityOtherLiquid'];
    eatingFruits = json['eatingFruits'];
    healthProblems = json['healthProblems'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['date'] = date;
    data['time'] = time;
    data['food'] = food;
    data['numberEaten'] = numberEaten;
    data['numberBowel'] = numberBowel;
    data['quantityWater'] = quantityWater;
    data['quantityOtherLiquid'] = quantityOtherLiquid;
    data['eatingFruits'] = eatingFruits;
    data['healthProblems'] = healthProblems;
    data['color'] = color;
    return data;
  }
}
